# Astro Commander
Astro Commander (working title) is an open-source (GPLv3) roguelike with probably vagually similar gameplay to FTL, although I have never actually played that game.

![Screenshot](http://i.imgur.com/qKKR043.png)

RUNNING
You will need Java to run it.


PLAYING
You control the crew of a ship and must try and avoid getting destroyed while going on an adventure.

Enter '?' for help.  Use numbers to select units, and arrow keys to move them.  Other controls are given in the game.


HINTS
* Log into the Control Panel.
* Hail the space station at the start.


STATUS
it works, but there's not much to it yet, and has not been thoroughly balanced.  And it probably has a few bugs.


CREDITS
Designed and programmed by Stephen Carlyle-Smith (Email: stephen.carlylesmith@googlemail.com  / Twitter: https://twitter.com/stephencsmith ).

