package com.scs.astrocommander.destinations;

import com.scs.astrocommander.Main;
import com.scs.astrocommander.asciieffects.ShipLaser;
import com.scs.astrocommander.map.AbstractMapSquare;

public abstract class AbstractAnotherShip extends AbstractSpaceLocation {

	public AbstractAnotherShip(Main _main, String _name) {
		super(_main, _name);
	}


}
