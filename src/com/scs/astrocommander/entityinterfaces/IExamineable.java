package com.scs.astrocommander.entityinterfaces;

public interface IExamineable {
	
	String getExamineText();

}
