package com.scs.astrocommander.entityinterfaces;

public interface IWearable {

	String getName();
	
	boolean giveOxygen();
}
